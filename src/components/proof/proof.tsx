import React, { useState, useEffect } from 'react';
import axios from 'axios';

const SINDRI_API_KEY = process.env.REACT_APP_SINDRI_API_KEY ? process.env.REACT_APP_SINDRI_API_KEY : '';
const axiosClient = axios.create({
    baseURL: 'https://forge.sindri.app/api/v1',
    headers: {
        Authorization: `Bearer ${SINDRI_API_KEY}`,
    }
});
  
  axiosClient.defaults.validateStatus = function (status) {
    return status >= 200 && status < 300
  }
  
  function useProofGeneration(circuitId:any, proofInput:any) {
    const [proofDetail, setProofDetail] = useState(null);
    const [error, setError] = useState("");
  
    useEffect(() => {
      const generateProof = async () => {
        try {
          const { data: proveResponse } = await axiosClient.post('/circuit/' + circuitId + '/prove', {
            proof_input: JSON.stringify(proofInput)
          });
  
          const proofId = proveResponse.proof_id;
          const startTime = Date.now();
  
          const checkProofStatus = async () => {
            while (true) {
              const { data: proofDetailResponse } = await axiosClient.get('/proof/' + proofId + '/detail');
              console.log(proofDetailResponse, 'proofDetailResponse');
              if (proofDetailResponse.status === 'Ready') {
                setProofDetail(proofDetailResponse);
                break;
              } else if (proofDetailResponse.status === 'Failed') {
                console.error(`Proof generation failed: ${proofDetailResponse.error}`);
                setError("Proof generation failed");
              } else if (Date.now() - startTime > 30 * 60 * 1000) { // 30 minutos
                console.error('Proof generation timed out after 30 minutes.');
                setError("Proof generation timed out after 30 minutes.");
              }
              await new Promise(resolve => setTimeout(resolve, 1000));
            }
          };
  
          checkProofStatus();
        } catch (err:any) {
          setError(err.message);
        }
      };
  
      if (circuitId && proofInput) {
        generateProof();
      }
    }, [circuitId, proofInput]);
  
    return { proofDetail, error };
  }
  
  function Proof({ circuitId, proofInput, _setSendForm }:any) {
    const { proofDetail, error }:any = useProofGeneration(circuitId, proofInput);

    if(proofDetail?.status === "Ready") {
      _setSendForm(proofDetail.proof.proof);
    }
    //console.log(proofDetail, 'proofDetail');
    //console.log(error, 'error');
    if (error) {
      return <div>
        {/* Error message */}
        <p className="mb-4 inline-flex text-sm font-semibold py-1 px-3 m-2 text-red-600 bg-red-200 rounded-full mb-4">
          Error: {error}
        </p>
      </div>;
    }
  
    if (!proofDetail) {
      return <div>
        {/* Generating proof */}
        <p className="mb-4 inline-flex text-sm font-semibold py-1 px-3 m-2 text-orange-600 bg-orange-200 rounded-full mb-4">
          Generating proof...
        </p>
      </div>;
    }
  
    return <div>
      {/* Success message */}
      <p className="mb-4 inline-flex text-sm font-semibold py-1 px-3 m-2 text-green-600 bg-green-200 rounded-full mb-4">
        Your request has been sent {proofDetail.status}
      </p>
    </div>;
  }
  
  export default Proof;