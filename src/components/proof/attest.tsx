import { EAS, SchemaEncoder } from "@ethereum-attestation-service/eas-sdk";
import { ethers } from "ethers";
  
function Attest({ proof, proofInput, pqrs }:any) {
  const provider = new ethers.BrowserProvider(window.ethereum)
 
  const getEthereumHash = (hexString:string) => {
    if (!hexString.startsWith('0x')) {
      hexString = `0x${hexString}`;
    }
    return hexString;
  };

  const connectWallet = async () => {
    try {
      const signer = await provider.getSigner();
      const EASContractAddress = process.env.REACT_APP_EAS ? process.env.REACT_APP_EAS : '';
      const eas = new EAS(EASContractAddress);
      console.log(signer, 'signer')
      eas.connect(signer);
      // Use the signer object for further actions

      // Initialize SchemaEncoder with the schema string
      const schemaEncoder = new SchemaEncoder("string Title, string Description, uint8 EntityID, bytes ZKProof");
      const encodedData = schemaEncoder.encodeData([
        { name: "Title", value: "Denuncia", type: "string" },
        { name: "Description", value: pqrs, type: "string" },
        { name: "EntityID", value: 131, type: "uint8" },
        { name: "ZKProof", value: getEthereumHash(proof), type: "bytes" },
      ]);
      
      const schemaUID = process.env.REACT_APP_SCHEMAUID ? process.env.REACT_APP_SCHEMAUID : '';
      
      const tx = await eas.attest({
        schema: schemaUID,
        data: {
          recipient: proofInput.address,
          revocable: false,
          data: encodedData,
        },
      });
      
      const newAttestationUID = await tx.wait();
      
      console.log("New attestation UID:", newAttestationUID);
      
    } catch (error) {
      console.error("Error connecting wallet:", error);
    }
  };
  
  connectWallet();
  

  return <div>
    {/* Success message */}
    <p className="mb-4 inline-flex text-sm font-semibold py-1 px-3 m-2 text-green-600 bg-green-200 rounded-full mb-4">
      Your request has been sent
    </p>
  </div>;
}

export default Attest;