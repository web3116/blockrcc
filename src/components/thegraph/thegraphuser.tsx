import React, { useState, useEffect } from 'react';
import { useQuery, gql } from '@apollo/client';
import { ethers } from "ethers";

const GET_ATTESTATION = gql`
  query Attestations($where: AttestationWhereInput) {
  attestations(where: $where) {
    id
    attester
    recipient
    refUID
    revocable
    revocationTime
    expirationTime
    data
    schemaId
  }
}
`;

export default function ThegraphUser(props:any) {
  const [attestationsData, setAttestationsData] = useState([]); // State to store attestation data
  const [loading, setLoading] = useState(false); // State to manage loading indicator
  const [error] = useState('');
  const schemaUID = process.env.REACT_APP_SCHEMAUID ? process.env.REACT_APP_SCHEMAUID : '';
  const { data, refetch } = useQuery(GET_ATTESTATION, {
    variables: {
      where: {
        schemaId: {
          equals: schemaUID, // Replace with your schema ID
        },
        AND: [
          {
            recipient: {
              equals: props.wallet,
            },
          },
        ],
      },
    },
  });

  useEffect(() => {
    if (data) {
      setAttestationsData(data?.attestations || []);
      setLoading(false);
    }

    if (error) {
      setLoading(false);
      console.log(error);
    }
  }, [data, error]);
  
  const handleRefetch = (props:any) => {
    setAttestationsData([]);
    refetch(props); // Refetch the data when the button is clicked
    setAttestationsData(data?.attestations || []);
  };

  
  const generateRandomNumber = () => {
    const randomNum = Math.floor(Math.random() * 100) + 1;
    return randomNum; // Return the random number instead of updating state
  };
  
  //console.log(data)
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error}</p>;
  const decoderesult:any=[];
  attestationsData.map((attestation:any) => {
      const abiCoder = new ethers.AbiCoder();
      const decodedData = abiCoder.decode(['string', 'string', 'uint8', 'bytes'], attestation.data);
      console.log(decodedData[1], 'decodedata');
      decoderesult.push({id:attestation.id, description: decodedData[1], title: decodedData[0], data:attestation.data});
      return true;
  })

  return (
    <div className="flex flex-col">
      <button className='btn text-white bg-purple-600 hover:bg-purple-700 w-full mb-4 sm:w-auto sm:mb-0' onClick={() => { handleRefetch(props) }}>Reload information</button>

      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
          <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table className="min-w-full divide-y divide-gray-200">
              <thead className="bg-gray-50">
                <tr>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Data user
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Title
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Status
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Description
                  </th>
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {decoderesult.length > 0 && decoderesult.description !== "" && decoderesult.map((deco:any) => (
                  <tr key={deco.id}>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <div className="flex items-center">
                        <div className="flex-shrink-0 h-10 w-10">
                          <img className="h-10 w-10 rounded-full cursor-pointer" src={`https://source.unsplash.com/random/200x200?sig=${generateRandomNumber()}`} title="anonymous" alt="anonymous" />
                        </div>
                        <div className="ml-4">
                          <div className="text-sm font-medium text-gray-900 cursor-pointer" title={deco.data}>{deco.data.substring(0, 20)}...</div>
                          <div className="text-sm text-gray-500 cursor-pointer" title={deco.id}>{deco.id.substring(0, 20)}...</div>
                        </div>
                      </div>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <div className="text-sm text-gray-900">{deco.title}</div>
                      {/*<div className="text-sm text-gray-500">{person.department}</div>*/}
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                        Active
                      </span>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500 cursor-pointer" title={deco.description}>{deco.description.substring(0, 150)}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}


