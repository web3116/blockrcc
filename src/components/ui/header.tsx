import MobileMenu from './mobile-menu'
import logo from '../../logo.svg';
import Login from '../auth/login';
import Logout from '../auth/logout';

export default function Header(props:any) {
  return (
    <header className="absolute w-full z-30">
      <div className="max-w-6xl mx-auto px-4 sm:px-6">
        <div className="flex items-center justify-between h-20">
          {/* Site branding */}
          <div className="shrink-0 mr-4">
            {/* Logo */}
            <a href="/" className="block" aria-label="Cruip">
              <img src={logo} className="w-12 h-12 fill-current text-purple-600"  alt="logo" />
            </a>
          </div>

          {/* Desktop navigation */}
          <nav className="hidden md:flex md:grow">
            {/* Desktop sign in as */}
            <ul className="flex grow justify-end flex-wrap items-center">
              <li>
                {props.userLogin ?
                  <Logout /> : <Login />
                }
                
              </li>
            </ul>
          </nav>

          <MobileMenu />

        </div>
      </div>
    </header>
  )
}
