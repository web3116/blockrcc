//import './Auth.css';
import {usePrivy} from '@privy-io/react-auth';

function Logout() {
  const {ready, authenticated, logout} = usePrivy();
  // Disable logout when Privy is not ready or the user is not authenticated
  const disableLogout = !ready || (ready && !authenticated);

  return (
    <button className="font-medium text-purple-600 hover:text-gray-200 px-4 py-3 flex items-center transition duration-150 ease-in-out" disabled={disableLogout} onClick={logout}>
      Log out
    </button>
  );
}

export default Logout;
