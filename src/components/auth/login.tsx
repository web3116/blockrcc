//import './Auth.css';
import {usePrivy} from '@privy-io/react-auth';

function Login(props:any) {
  const {ready, authenticated, login} = usePrivy();
  // Disable login when Privy is not ready or the user is already authenticated
  const disableLogin = !ready || (ready && authenticated);
  let _class = "font-medium text-purple-600 hover:text-gray-200 px-4 py-3 flex items-center transition duration-150 ease-in-out";
  if(props._class) {
    _class = props._class;
  }
  return (
    <button className={_class} disabled={disableLogin} onClick={login}>
      Log in
    </button>
  );
}

export default Login;
