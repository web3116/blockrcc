import { useState } from 'react';
import Proof from '../proof/proof';
import Seecomplaints from '../home/seecomplaints';
import Attest from '../proof/attest';

const CIRCUITID = process.env.REACT_APP_CIRCUITID ? process.env.REACT_APP_CIRCUITID : '';
export default function Pqrs(props:any) {
  const [proof, setProof] = useState<string>('')
  const [pqrs, setPqrs] = useState<string>('')
  const [sendForm, setSendForm] = useState<boolean>(false)
  const [sendAttest, setSendAttest] = useState<boolean>(false)
  const _setSendForm = (proof:string) => {
    setProof(proof);
    setSendForm(false);
    setSendAttest(true);
  };  
  const dataUser = {"cedula": 123456789, "address":props.user?.wallet?.address}
  
  const handleInputChange = (event:any) => {
    setPqrs(event.target.value);
  };

  const sendFormData = () => {
    if(pqrs === ''){
      alert('Please place your request');
    } else {
      setSendForm(true);
    }
   
  }

  //const dataUser = {"cedula": 123456789, "address": "0x34107Bce5EC357cE31739B84454c2c555a677568"}

  return (
  <section className='relative pt-32 pb-10 md:pt-40 md:pb-16'>
      {/* Section header */}
      <div className="max-w-3xl mx-auto text-center pb-12 md:pb-16">
        <div className="inline-flex text-sm font-semibold py-1 px-3 m-2 text-green-600 bg-green-200 rounded-full mb-4">Welcome {props.user.id}</div>
        <h1 className="h2 mb-4">Here you can leave your complaint</h1>
        <p className="text-xl text-gray-400">If you have experienced poor service or attention from a business, you have the right to file a complaint. This can help the business improve its customer service and prevent others from having similar experiences.</p>
      </div>
      <div className="max-w-xs mx-auto sm:max-w-none sm:flex sm:justify-center mb-10" data-aos="fade-up" data-aos-delay="600">
        {props.user?.wallet?.address && <Seecomplaints type="user" wallet={props.user?.wallet?.address} />}
      </div>
      <div className="max-w-6xl mx-auto px-4 sm:px-6">

        {/* CTA box */}
        <div className="relative bg-purple-600 py-10 px-8 md:py-16 md:px-12" data-aos="fade-up">

          <div className="relative flex flex-col lg:flex-row justify-between items-center">
            {/* CTA form */}
            <div className="w-full">
              <div className="flex flex-col sm:flex-row justify-center max-w-xs mx-auto sm:max-w-md lg:max-w-none">
                <input type="text" className="w-full appearance-none bg-purple-700 border border-purple-500 focus:border-purple-300 rounded-sm px-4 py-3 mb-2 sm:mb-0 sm:mr-2 text-white placeholder-purple-400" placeholder="Your ID number is?" aria-label="Your ID number is?" />
              </div>
              <div className="flex flex-col sm:flex-row justify-center max-w-xs mt-5 mx-auto sm:max-w-md lg:max-w-none">
                <textarea value={pqrs} onChange={handleInputChange}  className="w-full appearance-none bg-purple-700 border border-purple-500 focus:border-purple-300 rounded-sm px-4 py-3 mb-2 sm:mb-0 sm:mr-2 text-white placeholder-purple-400" placeholder="What is your complaint or claim?..."></textarea>
              </div>
              <div className=" flex flex-col sm:flex-row justify-center max-w-xs mx-auto mt-5 sm:max-w-md lg:max-w-none">
                <button className="btn text-purple-600 bg-purple-100 hover:bg-white shadow" onClick={() => { sendFormData() }}>SEND</button>
              </div>
              {sendForm &&
                <Proof circuitId={CIRCUITID} proofInput={dataUser} _setSendForm={_setSendForm} />
              }

              {sendAttest && <Attest circuitId={CIRCUITID} proofInput={dataUser} proof={proof} pqrs={pqrs} />}
              
            </div>

          </div>

        </div>

      </div>
    </section>
  )
}
