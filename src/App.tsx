import React, { useState } from 'react';
import {usePrivy} from '@privy-io/react-auth';
import Header from './components/ui/header';
import Home from './components/home/home';
import Pqrs from  './components/pqrs/pqrs'
export default function App() {
  const [userLogin, setUserLogin] = useState(false);
  const {ready, authenticated, user} = usePrivy();
  let conten;
  if (!ready) {
    // Do nothing while the PrivyProvider initializes with updated user state
    conten = <></>;
  }

  if (ready && !authenticated) {
    conten = <Home />;
  }

  if (ready && authenticated) {
    console.log(user)
    // Replace this code with however you'd like to handle an authenticated user
    if(!userLogin){
      setUserLogin(true);
    }
    conten = <Pqrs user={user} /> ;
  }

  return (
    <div className="flex flex-col min-h-screen overflow-hidden">
      <Header userLogin={userLogin} />
      {conten}
    </div>
  );
}
